#!/bin/sh
set -x
mkdir ~/systemready-cert-logs
cd ~/systemready-cert-logs
dmesg 2>&1 |tee dmesg.txt
lspci 2>&1 |tee lspci.txt
lspci -vvv 2>&1 |tee lspci-vvv.txt
cat /proc/interrupts 2>&1 |tee cat-proc-interrupts.txt
cat /proc/cpuinfo 2>&1 |tee cat-proc-cpuinfo.txt
cat /proc/meminfo 2>&1 |tee cat-proc-meminfo.txt
lscpu 2>&1 |tee lscpu.txt
lsblk 2>&1 |tee lsblk.txt
lsusb 2>&1 |tee lsusb.txt
dmidecode 2>&1 |tee dmidecode.txt
dmidecode --dump-bin dmidecode.bin
uname -a 2>&1 |tee uname-a.txt
cat /etc/os-release 2>&1 |tee cat-etc-os-release.txt
date 2>&1 |tee date.txt
timedatectl 2>&1 |tee timedatectl.txt
hwclock 2>&1 |tee hwclock.txt
efibootmgr 2>&1 |tee efibootmgr.txt
efibootmgr -t 20 2>&1 |tee efibootmgr-t-20.txt
efibootmgr -t 5 2>&1 |tee efibootmgr-t-5.txt
efibootmgr -c 2>&1 |tee efibootmgr-c.txt
ifconfig 2>&1 |tee ifconfig.txt
ip addr show 2>&1 |tee ip-addr-show.txt
ping -c 5 www.arm.com 2>&1 | tee ping-c-5-www-arm-com.txt
if command -v apt &> /dev/null; then
    apt-get update
    apt-get install -y acpica-tools
elif command -v yum &> /dev/null; then
    yum check-update
    yum install -y acpica-tools
elif command -v zypper &> /dev/null; then
    zypper modifyrepo --all -e
    zypper refresh
    zypper install acpica
elif command -v dnf &> /dev/null; then
    dnf check-update
    dnf install -y acpica-tools
else
    echo "Unknown package manager"
fi
acpidump > acpi.log
acpixtract -a acpi.log
iasl -d *.dat
date --set="20221215 05:30" 2>&1 |tee date-set-202212150530.txt
date 2>&1 |tee date-after-set.txt
hwclock --set --date "2023-01-01 09:10:15" 2>&1 |tee hw-colock-set-20230101091015.txt
hwclock 2>&1 |tee hwclock-after-set.txt
cp -r /sys/firmware ~/systemready-cert-logs/
cd ~/systemready-cert-logs
